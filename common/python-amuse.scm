(define-module (common python-amuse)
             #:use-module ((guix licenses) #:prefix license:)
             #:use-module (gnu packages)
             #:use-module (guix packages)
             #:use-module (guix utils)
             #:use-module (guix gexp)
             #:use-module (guix download)
             #:use-module (guix git-download)
             #:use-module (guix build-system python)
             #:use-module (guix build-system gnu)
             #:use-module (gnu packages base)
             #:use-module (gnu packages gcc)
             #:use-module (gnu packages perl)
             #:use-module (gnu packages node)
             #:use-module (gnu packages python)
             #:use-module (gnu packages algebra)
             #:use-module (gnu packages python-web)
             #:use-module (gnu packages commencement)
             #:use-module (gnu packages python-science)
             #:use-module (gnu packages xml)
             #:use-module (gnu packages cmake)
             #:use-module (gnu packages multiprecision)
             #:use-module (gnu packages maths)
             #:use-module (gnu packages elf)
             #:use-module (gnu packages glib)
             #:use-module (gnu packages databases)
             #:use-module (gnu packages check)
             #:use-module (gnu packages protobuf)
             #:use-module (gnu packages mpi)
             #:use-module (gnu packages ssh)
             #:use-module (gnu packages gtk)
             #:use-module (gnu packages machine-learning)
             #:use-module (gnu packages bootstrap)
             #:use-module (gnu packages pkg-config)
             #:use-module (gnu packages python-check)
             #:use-module (gnu packages python-crypto)
             #:use-module (gnu packages python-xyz)
             #:use-module (gnu packages time)
             #:use-module (gnu packages libidn)
             #:use-module (gnu packages libffi)
             #:use-module (gnu packages pulseaudio)
             #:use-module (gnu packages python-build)
             #:use-module (gnu packages compression)
             #:use-module (gnu packages bioinformatics)
             #:use-module (gnu packages geo))


(define-public python-amuse-framework
  (package
    (name "python-amuse-framework")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-framework" version))
              (sha256
               (base32
                "0wshdaq5ncr20pmh5hf0clv66075xzj9qcg6iacg2nnr4hlpk7ys"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (inputs (list gcc-toolchain
			 gfortran-toolchain
			 cmake
			 hdf5
			 mpich
			 python
			 netcdf
       lapack
       openblas))
    (propagated-inputs (list python-docutils
                             python-h5py
                             python-mpi4py-mpich
                             python-numpy
                             python-pip
                             python-pytest
                             python-setuptools
                             python-setuptools-scm
                             python-wheel))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment")
    (description "The Astrophysical Multipurpose Software Environment")
    (license #f)))

(define-public openblas-omp
  (package/inherit openblas
    (name "openblas-omp")
    (supported-systems '("x86_64-linux" "aarch64-linux" "mips64el-linux"
                         "powerpc64le-linux"))
    (arguments
     (substitute-keyword-arguments (package-arguments openblas)
       ((#:make-flags flags #~'())
        #~(append (list "USE_OPENMP=1")
                 #$flags))))
    (synopsis "Optimized BLAS library with OpenMP Support")
    (license license:bsd-3)))


(define-public phantom-src
  (package
    (name "phantom-src")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri "https://github.com/rieder/phantom/archive/ca6907f5f9a95f866b1d7e520cc356ab8cec8dd0.tar.gz")
              (sha256
               (base32
                "1alwl4kipp2whclw7428pngv738jsvhlvn29k6q18ddd8j6q24cx"))))
    (build-system gnu-build-system)
    (native-inputs (list python))
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'configure)
           (delete 'build)
           (delete 'sanity-check)
           (delete 'check)
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
                      (let* ((out (assoc-ref outputs "out")))
                             (copy-recursively "." out)))))))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - phantom sources")
    (description "The Astrophysical Multipurpose Software Environment - phantom sources")
    (license #f)))

(define-public python-amuse-phantom
  (package
    (name "python-amuse-phantom")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-phantom" version))
              (sha256
               (base32
                "029nqwrybfjhlkxnf7k054s5l77m4q1y9kyrakn53jj3fhylq3wc"))))
              ;(patches (search-patches "phantom.patch"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
           (add-after 'unpack 'patch-make
                      (lambda* (#:key inputs outputs #:allow-other-keys)
                               (let* ((phantom (assoc-ref inputs "phantom-src")))
                              (copy-recursively phantom "./phantom-src")
                              (chdir "./phantom-src")
                              (for-each make-file-writable (find-files "."))
                              (chdir "../")
                              (substitute* "src/amuse/community/phantom/Makefile"
                                            (("\\$\\(DOWNLOAD_FROM_WEB\\)") 
                                              "cp -r ../../../../phantom-src src/phantom")))))
           (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
               (setenv "SHELL" (which "sh"))))
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/"))))))
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
                  mpich
                  phantom-src
                  gfortran-toolchain
                  ))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - phantom")
    (description "The Astrophysical Multipurpose Software Environment - phantom")
    (license #f)))

(define-public python-amuse
  (package
    (name "python-amuse")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse" version))
              (sha256
               (base32
                "0mgi7zfsms8kmw94jja613hv2xpdrdxc8p0dppz8fmbpkdhywpyi"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-phantom
                             python-amuse-framework
                             python-amuse-petar
                             python-matplotlib
                             python-numpy))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment")
    (description "The Astrophysical Multipurpose Software Environment")
    (license #f)))


(define-public petar-src
  (package
    (name "petar-src")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri "https://github.com/lwang-astro/PeTar/archive/22437b74c6541fc9451607fb2933558bd6924a41.tar.gz")
              (sha256
               (base32
                "1anq2x0kc59rbv0a1n36pjzvaa0g73i0i5fy2jw7vav3f8wscgn9"))))
    (build-system gnu-build-system)
    (native-inputs (list python))
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
	   (delete 'configure)
	   (delete 'build)
           (delete 'sanity-check)
           (delete 'check)
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
		      (let* ((out (assoc-ref outputs "out")))
			     (copy-recursively "." out)))))))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - PeTar")
    (description "The Astrophysical Multipurpose Software Environment - PeTar")
    (license #f)))


(define-public fdps-src
  (package
    (name "fdps-src")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri "https://github.com/FDPS/FDPS/archive/6fedb4b8bd7a504598e83a4189a7a83c533a0848.tar.gz")
              (sha256
               (base32
                "0f58kzkw6i6v34x1s7lakcqi0hxqns9vrmy7mc7h7iz3c719gzrf"))))
    (build-system gnu-build-system)
    (native-inputs (list python))
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
	   (delete 'configure)
	   (delete 'build)
           (delete 'sanity-check)
           (delete 'check)
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
		      (let* ((out (assoc-ref outputs "out")))
			     (copy-recursively "." out)))))))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - PeTar")
    (description "The Astrophysical Multipurpose Software Environment - PeTar")
    (license #f)))

(define-public sdar-src
  (package
    (name "sdar-src")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri "https://github.com/lwang-astro/SDAR/archive/cebf0a9cbd17111c6917e1120452bb9f661e33b0.tar.gz")
              (sha256
               (base32
                "0m38cxl9vcm09pbm0704cv6vwmnwhha824rcp0lvabzjin95597b"))))
    (build-system gnu-build-system)
    (native-inputs (list python))
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
	   (delete 'configure)
	   (delete 'build)
           (delete 'sanity-check)
           (delete 'check)
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
		      (let* ((out (assoc-ref outputs "out")))
			     (copy-recursively "." out)))))))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - PeTar")
    (description "The Astrophysical Multipurpose Software Environment - PeTar")
    (license #f)))

(define-public python-amuse-petar
  (package
    (name "python-amuse-petar")
    (version "2023.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "amuse-petar" version))
              (sha256
               (base32
                "165wlw8276011skq5dsy8lyw7li5c1bpaw3s0lark88lqw11d28m"))
	      ;(patches (search-patches "petar.patch"))
        ))
    (build-system python-build-system)
    (arguments
      '(#:phases
         (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
           (delete 'fix-and-disable-failing-tests)
           (delete 'sanity-check)
           (delete 'check)
	   (add-after 'unpack 'patch-make
		      (lambda* (#:key inputs outputs #:allow-other-keys)
            		       (let* ((petar (assoc-ref inputs "petar-src"))
				                      (fdps (assoc-ref inputs "fdps-src"))
				                      (sdar (assoc-ref inputs "sdar-src")))
                              (copy-recursively petar "./petar-src")
                              (chdir "./petar-src")
                              (for-each make-file-writable (find-files "."))
                              (chdir "../")
                              (copy-recursively fdps "./fdps-src")
                              (chdir "./fdps-src")
                              (for-each make-file-writable (find-files "."))
                              (chdir "../")
                              (copy-recursively sdar "./sdar-src")
                              (chdir "./sdar-src")
                              (for-each make-file-writable (find-files "."))
                              (chdir "../")
				                      (substitute* "src/amuse/community/petar/Makefile"
                              (("\\$\\(DOWNLOAD_FROM_WEB\\)")
                                "cp -r ../../../../petar-src src/PeTar
\tcp -r ../../../../fdps-src src/FDPS
\tcp -r ../../../../sdar-src src/SDAR")))))
	   (add-before 'build 'pre-configure
             (lambda _
               (setenv "CONFIG_SHELL" (which "sh"))
	       (setenv "SHELL" (which "sh"))))
	   (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (invoke "python" "setup.py" "install"
                 (string-append "--prefix=" (assoc-ref outputs "out"))
                                "--root=/")))))) 
    (propagated-inputs (list python-amuse-framework))
    (inputs (list gcc-toolchain
		  mpich
		  sdar-src
		  fdps-src
		  petar-src))
    (home-page "http://www.amusecode.org/")
    (synopsis "The Astrophysical Multipurpose Software Environment - PeTar")
    (description "The Astrophysical Multipurpose Software Environment - PeTar")
    (license #f)))
